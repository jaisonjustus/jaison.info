# jaison.info

### How to create and build playlists?
1. create a .yml playlist file in `scripts/playlists` folder
2. register the newly created file on `scripts/playlist.js` file
3. run `$ node build_playlist.js`

### How to generate track embed?
1. goto `tools` folder
2. run `$ node discogs <release_id> <track_no,track_no...trac_no>` 